#ifndef _CONTAININER_H
#define _CONTAININER_H


#include <string>
#include <vector>
#include <stdexcept>
#include <iostream>
#include <cstdint>
using namespace std;

const int MAX_NUM = 200;

//class outofrange : public std::runtime_error
//{
//public:
//  outofrange() : std::runtime_error( "out_of_range" )
//  {
//  }
//};

class Container
{
public:
  Container();
  Container(int m_size);
  ~Container();

  Container(const Container& that);
  int get_size();
  int add_item(const string& str);
  string get_item(int index);
  bool remove_item(int index);

  bool operator==(const Container& that);  
  friend ostream& operator<<(ostream& output, const Container& that);
private:
  int m_size;
  vector<string> m_vec;
};

#endif