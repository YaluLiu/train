// FirstTestClass.cpp : Defines the entry point for the console application.
//

#include "Container.h"
#include <stdexcept>
#include <gtest/gtest.h>

TEST(test_get_size, Demo)
{
  Container*  c_ptr =new Container(10);
  EXPECT_EQ(10, c_ptr->get_size());
}

TEST(test_add_item, Demo)
{
  Container*  c_ptr =new Container;
  c_ptr->add_item("I'm super man No.1!");
  c_ptr->add_item("I'm super man No.2!");
  cout<<*c_ptr<<endl;
  EXPECT_EQ(2, c_ptr->get_size());

  EXPECT_EQ(-1,c_ptr->add_item("I'm super man No.2!"));
  cout<<*c_ptr<<endl;
}
TEST(test_get_item, Demo)
{
  Container*  c_ptr =new Container;
  c_ptr->add_item("I'm super man No.1!");
  c_ptr->add_item("I'm super man No.2!");


  EXPECT_STREQ("I'm super man No.1!",c_ptr->get_item(0).c_str());
  EXPECT_STREQ("I'm super man No.2!",c_ptr->get_item(1).c_str());

  cout<<"EXCEPT1!"<<endl;
  EXPECT_ANY_THROW(c_ptr->get_item(-1));
  cout<<"EXCEPT2!"<<endl;
  EXPECT_NO_THROW(c_ptr->get_item(16));
}
TEST(test_remove_item, Demo)
{
  Container*  c_ptr =new Container;
  c_ptr->add_item("I'm super man No.1!");
  c_ptr->add_item("I'm super man No.2!");


  EXPECT_TRUE(c_ptr->remove_item(0));
  EXPECT_FALSE(c_ptr->remove_item(10));
}
int main(int argc, char** argv)
{
    //testing::GTEST_FLAG(output) = "xml:";

    // Add the global test environment
    //testing::AddGlobalTestEnvironment(new MoneyTestEnvironment);

    testing::InitGoogleTest(&argc, argv);

    //testing::FLAGS_gtest_death_test_style = "fast";

    return RUN_ALL_TESTS();
}

