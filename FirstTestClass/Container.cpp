#include "Container.h"
#include <exception>

using namespace std;

Container::Container()
{
  m_size = 0;
  vector<string> m_vec(m_size);
}
Container::Container(int m_size = 0) : m_size(m_size)
{
  vector<string> m_vec(m_size);
}

int Container:: get_size(void)
{
  return m_size;
}
int  Container:: add_item(const string& str)
{
  vector<string>::iterator it;
  for(it=m_vec.begin();it!=m_vec.end();it++)
    if(*it == str)
      return -1;
  m_vec.push_back(str);
  return m_size++;
}
string Container:: get_item(int index)
{
  if((index >= m_size) || (index < 0))
    throw out_of_range("OUT_OF_RANGE");

  return m_vec[index];
}
bool Container:: remove_item(int index)
{
  if((index >= m_size) || (index < 0))
    return false;
  m_vec.erase(m_vec.begin()+index);
  return true;
}

ostream& operator<<(ostream& output, const Container& that)
{
  for(int i = 0; i < that.m_size; ++i)
    output << that.m_vec[i] <<"  ";

  return output;
}
